# STM32WB355RG

## Introduction

The STM32WB355RG is a microcontroller from STMicroelectronics that combines an Arm Cortex-M4 core with a Cortex-M0+ core. It is part of the STM32WB series, which is designed for wireless applications.

## Features

- Arm Cortex-M4 core running at up to 64 MHz
- Arm Cortex-M0+ core running at up to 32 MHz
- 1 MB Flash memory
- 256 KB RAM
- Bluetooth Low Energy (BLE) 5.0
- IEEE 802.15.4 radio
- USB 2.0 Full-Speed
- Multiple communication interfaces (UART, SPI, I2C, etc.)
- Analog peripherals (ADC, DAC, etc.)
- Timers and PWM outputs
- Low-power modes for energy efficiency

## Getting Started

To get started with the STM32WB355RG microcontroller, you will need the following:

- [STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html) software development platform
- [STM32CubeProgrammer](https://www.st.com/en/development-tools/stm32cubeprog.html) to check the firmware
- [STM32WB Nucleo](https://www.st.com/en/evaluation-tools/p-nucleo-wb55.html) board or custom hardware based on the STM32WB355RG


Follow these steps to start developing with the STM32WB355RG:

1. Install the STM32Cube softwares development platform.
2. Set up your development environment.
3. Check and udpate the firmware of the STM32WB355RG with the STM32CubeProgrammer.
4. Start developing your application using the STM32CubeIDE, here we used the exemple code provided by STMicroelectronics.

## Documentation

For detailed information about the STM32WB355RG microcontroller, refer to the following resources:

- [Datasheet](https://www.st.com/en/microcontrollers-microprocessors/stm32wb55rg.html)

## Community and Support

If you have any questions or need assistance with the STM32WB355RG microcontroller, you can reach out to the community and get support from the following resources:

- [Official STMicroelectronics Community](https://community.st.com/)
- [STMicroelectronics Technical Support](link-to-technical-supporthttps://www.st.com/content/st_com/en/support/support-home.html)

## Conclusion

The STM32WB355RG is a powerful microcontroller that offers a wide range of features for wireless applications. With the resources and support available, you can start developing your own projects using this microcontroller.
