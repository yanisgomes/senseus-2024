The code of the android application is available [here](https://gitlab.com/yanisgomes/senseus-2024/-/tree/main/docs/files/CreaRateApp)

We used Xamarin which is a cross-platform mobile app development framework that allows us to create Android and iOS applications using C# and .NET.

# BLE and Xamarin

We used [this](https://github.com/dotnet-bluetooth-le/dotnet-bluetooth-le) plugin to manage the BLE connection. It is a cross-platform Bluetooth Low Energy plugin for Xamarin. 

Further details on how Xamarin works can be found on the [official documentation site](https://learn.microsoft.com/fr-fr/previous-versions/xamarin/get-started/what-is-xamarin)


# XAML

XAML (eXtensible Application Markup Language) is a declarative XML-based language used for initializing structured values and objects. It is the main langage used to develop application with Xamarin. XAML enables developers to define user interfaces in a clear, hierarchical manner, separating the UI design from the underlying business logic.

- _Declarative Syntax_: XAML allows developers to describe the layout and behavior of a UI in a straightforward, readable format.
- _Separation of Concerns_: By using XAML for UI design and C# or VB.NET for logic, it promotes a clean separation between the presentation layer and the business logic.
- _Hierarchical Structure_: XAML’s hierarchical structure makes it easy to nest elements, define complex layouts, and manage resources.
- _Integration with .NET_: XAML is tightly integrated with the .NET framework, making it easy to bind data, handle events, and manage application states.