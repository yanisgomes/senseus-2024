﻿using CreaRateApp.Services;
using CreaRateApp.Views;
using Plugin.BLE.Abstractions.Contracts;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CreaRateApp
{
    public partial class App : Application
    {
        // Variables globales
        public static IDevice detectedDevice;
        public static bool connectedDevice = false;
        public static IService heartRateService;
        public static ICharacteristic heartRateMeasurement;
        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();
            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
