﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;



namespace CreaRateApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VueSuivi : ContentPage
    {
        private ushort heartRateValue;
        private byte[] heartRateValueArray = new byte[4] {0,0,0,0};

        public VueSuivi()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (App.connectedDevice)
                StartNotify();
            
        }

        private async void StartNotify()
        {
            // Fonction qui se lance  chaque réception de notification
            App.heartRateMeasurement.ValueUpdated += (senders, args) =>
            {
                UpdateCreaRate();
            };
            // Active les notifications BLE
            await App.heartRateMeasurement.StartUpdatesAsync();
        }

        private void UpdateCreaRate()
        {
            // Reception de la valeur
            Array.Copy(App.heartRateMeasurement.Value, heartRateValueArray, 4);
            heartRateValue = BitConverter.ToUInt16(heartRateValueArray, 1);

            // Thread principale pour modifier les éléments graphiques  
            Device.BeginInvokeOnMainThread(() =>
            {
                CreaValue.Text = heartRateValue.ToString();
            });
        }
    }
}