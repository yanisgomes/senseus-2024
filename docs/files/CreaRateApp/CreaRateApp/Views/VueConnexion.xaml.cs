﻿using Plugin.BLE;
using Plugin.BLE.Abstractions;
using Plugin.BLE.Abstractions.Contracts;
using Plugin.BLE.Abstractions.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CreaRateApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VueConnexion : ContentPage
    {
        //BLE
        IBluetoothLE ble;
        IAdapter adapter;

        //UX
        ActivityIndicator scanningIndicator = new ActivityIndicator();

        // UUID Crea Rate
        byte[] hr_UUID = { 0x18, 0x0D };

        // Infos connexion 
        Label titre = new Label { Text = "Connexion établie", 
            HorizontalTextAlignment=TextAlignment.Start, 
            FontSize=Device.GetNamedSize(NamedSize.Large, typeof(Label))
        };
        Label deviceConnectedLabel = new Label { Text = "Appareil connecté",
            FontSize = Device.GetNamedSize(NamedSize.Body, typeof(Label))
        };
        Label deviceNameLabel = new Label();
        Label deviceAdressLabel = new Label();
        public VueConnexion()
        {
            InitializeComponent();

            // Config BLE Scan
            ble = CrossBluetoothLE.Current;
            adapter = ble.Adapter;
            adapter.ScanTimeout = 5000; // Timeout en ms
         }


        // Fonction scan button clicked

        private async void Button_Clicked(object sender, EventArgs e)
        {
            // Fonction qui se lance lors de la détection d'un appareil
            adapter.DeviceDiscovered += async (s, a) =>
            {
                // L'appareil a un nom
                if (a.Device.Name != null)
                {
                    // Advertisement Records
                    foreach (var record in a.Device.AdvertisementRecords)
                    {
                        // Champ UUID 16 bits
                        if (record.Type == Plugin.BLE.Abstractions.AdvertisementRecordType.UuidsIncomple16Bit)
                        {
                            // Correspond à l'UUID hr
                            if (record.Data.SequenceEqual(hr_UUID))
                            {
                                App.detectedDevice = a.Device;
                                await adapter.StopScanningForDevicesAsync();
                                await connectionRequest();

                            }
                        }
                    }
                }
            };
            // Status bluetooth (on/off)
            var state = ble.State;
            if (state == BluetoothState.Off)
            {
                await DisplayAlert(null, "Bluetooth non activé", "Quitter");
            }
            else if (state == BluetoothState.On)
            {
                if (!adapter.IsScanning)
                {
                    //UX
                    MainStack.Children.Remove(ScanButton);
                    scanningIndicator.IsRunning = true;
                    scanningIndicator.HorizontalOptions = LayoutOptions.Fill;
                    scanningIndicator.VerticalOptions = LayoutOptions.CenterAndExpand;
                    MainStack.Children.Add(scanningIndicator);

                    // Scanning
                    await adapter.StartScanningForDevicesAsync();

                    // UX
                    scanningIndicator.IsRunning = false;
                    MainStack.Children.Remove(scanningIndicator);
                    MainStack.Children.Add(ScanButton);
                }
            }
            
        }
        private async Task connectionRequest()
        {
            // UX
            scanningIndicator.IsRunning = false;
            MainStack.Children.Remove(scanningIndicator);

            // fenetre contextuelle
            string messageConnexion = "Se connecter à l'appareil : \n" + App.detectedDevice.NativeDevice.ToString();
            bool result = await DisplayAlert("Senseur détecté", messageConnexion, "Se connecter", "Annuler");
            if (result)
            {
                // Etablissement de la connexion
                scanningIndicator.IsEnabled = true;
                MainStack.Children.Add(scanningIndicator);
                await AskDeviceForConnection();
            }
            else
            {
                // Bouton de base
                MainStack.Children.Add(ScanButton);
            }
        }

        private async Task AskDeviceForConnection()
        {
            try
            {
                // UX
                scanningIndicator.IsRunning = false;
                MainStack.Children.Remove(scanningIndicator);

                // Connexion fonctionne
                await adapter.ConnectToDeviceAsync(App.detectedDevice);
                await DisplayAlert(null, "Connexion réussie", "Continuer");
                App.connectedDevice = true;
                DiscoverServicesAndCharacteristicsAsync();
                DisplayConnectionInfo();
            }
            catch (DeviceConnectionException)
            {
                await DisplayAlert(null, "Connexion échouée", "Quitter");
                MainStack.Children.Add(ScanButton);
            }
        }

        private void DisplayConnectionInfo()
        {
            deviceNameLabel.Text = App.detectedDevice.Name;
            deviceAdressLabel.Text = App.detectedDevice.NativeDevice.ToString();

            MainStack.Children.Add(titre);
            MainStack.Children.Add(deviceAdressLabel);
            MainStack.Children.Add(deviceNameLabel);
            MainStack.Children.Add(deviceAdressLabel);
        }

        private async void DiscoverServicesAndCharacteristicsAsync()
        {
            var services = await App.detectedDevice.GetServicesAsync();
            foreach (var service in services)
            {
                // Service HR
                if (service.Name == "Heart Rate")
                {
                    App.heartRateService = service; 

                    var characts = await service.GetCharacteristicsAsync();
                    foreach (var charac in characts)
                    {
                        // Caractéristique HR
                        if (charac.Name == "Heart Rate Measurement")
                        {
                            App.heartRateMeasurement = charac;
                        }
                    }
                }
            }
        }
    }
}