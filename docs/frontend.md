# Biotech Android Application

This document provides an overview of the Android application developed for the biotech project. The application is designed to receive transmitted data from an STM32 microcontroller after it has been processed by an ADuCM355 chip.

The application is available [here](https://gitlab.com/yanisgomes/senseus-2024/-/blob/main/docs/files/CreaRate.apk).

## Overview

The biotech Android application serves as a user interface for the biotech project. It allows users to interact with the transmitted data and view relevant information.

![Biotech Android Application](files/images/screen_creaRate.png)

## Features

1. Data Reception: The mobile application receives data transmitted from the STM32 microcontroller via a wireless connection (Bluetooth Low Energy).

2. Data Processing: The received data is processed by the ADuCM355 chip to extract relevant information.

3. User Interface: The application provides a user-friendly interface to display the processed data and allow users to interact with it.

4. Real-time Updates: The application updates the displayed data in real-time as new data is received from the microcontroller.

## Architecture

The Android application follows a client-server architecture, where the STM32 microcontroller acts as the server and the Android device acts as the client. The application communicates with the microcontroller using a wireless connection (BLE).

## Technologies Used

- We used Xamarin which is a cross-platform mobile app development framework that allows us to create Android and iOS applications using C# and .NET.

## Installation

To install the biotech Android application, follow these steps:

1. Download the APK file from the project's website.

2. Enable installation from unknown sources in the Android device settings.

3. Open the APK file and follow the on-screen instructions to install the application.

## Usage

Once installed, launch the biotech Android application on your device. Click on the 'Connexion" window. Click on scan. If the device is found, follow the instruction to connect to the device. Once connected, the application will start receiving data from the microcontroller which are printed in the "Suivi" window.

## Conclusion

The biotech Android application provides a convenient way to interact with the transmitted data from the biotech project. It offers real-time updates and a user-friendly interface for data visualization and interaction.

For more detailed information, refer to the project's documentation and source code.
