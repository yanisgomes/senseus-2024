# BLE

## BLE Protocol

BLE allows multiple "profiles" which are predefined communication protocols. We will use the "Heart Rate" profile that enables the transfer of heart rate data. There are more suitable profiles for our application, such as the [Generic Health Sensor](https://www.bluetooth.com/specifications/specs/generic-health-sensor-profile/) profile, but there is no example code available for this profile.

## BLE Protocol

BLE is a protocol that defines how data should be structured. It is divided into several sub-protocols, each with a well-defined role. These sub-protocols can be broken down into several layers that make up the BLE stack.

![Stack-BLE](files/images/BLE_layers.png)

### The Physical Layer
The Physical Layer (PHY) refers to the physical radio used for communication and for modulating/demodulating data. It operates in the ISM band (2.4 GHz spectrum).

### The Link Layer
The Link Layer interacts with the Physical Layer (radio) and provides higher levels with an abstraction and means to interact with the radio (via an intermediary layer called HCI that we will cover soon). It is responsible for managing the radio state as well as the timing requirements to comply with the Bluetooth Low Energy specification.

### The Host Controller Interface (HCI)
The HCI (Host Controller Interface) layer is a standard protocol defined by the Bluetooth specification that allows the host layer to communicate with the controller layer. These layers can exist on separate chips or on the same chip.

### The Logical Link Control and Adaptation Protocol (L2CAP)
The L2CAP layer acts as a protocol multiplexing layer. It takes several protocols from the upper layers and places them into standard BLE packets. These packets are then passed down to the lower layers.

### The Attribute Protocol (ATT)
The Attribute Protocol (ATT) defines how a server exposes its data to a client and how that data is structured.

### The Generic Attribute Profile (GATT)
The Generic Attribute Profile (GATT) defines the format of the data exposed by a BLE device. It also defines the procedures required to access the data exposed by a device.

There are two roles in GATT: Server and Client. The server is the device that exposes the data it controls or contains and potentially other aspects of its behavior that other devices may control.

A client, on the other hand, is the device that interacts with the server to read the data exposed by the server and/or control the server's behavior. Keep in mind that a Bluetooth LE device can act as both a server and a client at the same time.

### Services and Characteristics

Services are a grouping of one or more attributes (a generic term for any type of data exposed by the server). They aim to group related attributes that provide a specific functionality on the server. For example, the battery service adopted by the SIG (Special Interest Group that defines Bluetooth standards) contains a characteristic called "Battery Level."

A characteristic is always part of a service and represents a piece of information or data that a server wants to expose to a client. For example, the "Battery Level" characteristic represents the remaining power level of a battery in a device that can be read by a client.

In BLE, there are six types of operations on characteristics:

1. **Commands**
2. **Requests**
3. **Responses**
4. **Notifications**
5. **Indications**
6. **Confirmations**
