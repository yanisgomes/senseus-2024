# AgroSense Team : SenseUs 2024

Welcome to the documentation of the AgroSense team's project for the SenseUs competition edition 2024 ! This project aims to develop a continuous sensor for creatinine levels, specifically designed for medical applications. Leveraging advanced technology, we utilize the ADuCM355 for precise amperometric measurements and an STM32WBRG with an embedded BLE module for data transmission.

## Project Overview

### Introduction

The AgroSense project is focused on creating a reliable and continuous sensor system for monitoring creatinine levels. This is crucial for patients requiring constant monitoring of kidney function. Our system uses a combination of advanced microcontrollers and Bluetooth technology to ensure accurate measurements and seamless data transmission.

### Objectives

- **Accurate Measurements**: Utilizing the ADuCM355 for precise amperometric readings.
- **Real-time Data Processing**: Processing data using an STM32 microcontroller.
- **Wireless Communication**: Transmitting data via Bluetooth Low Energy (BLE) to a mobile application.
- **User-Friendly Interface**: Providing an easy-to-use application for end-users to monitor creatinine levels.

## Documentation Structure

The documentation for this project is organized as follows:

1. **About**: Overview and background information about the project.
2. **Team**: Introduction to the team members working on the project.
3. **Project**: Detailed information about the project's goals and implementation.
4. **Documentation**:
    - **Microfluidics**:
        - **Design**: Information about the design of the microfluidic circuit.
        - **Fabrication**: Steps and processes involved in fabricating the microfluidic circuit.
        - **Testing**: Methods and results of testing the microfluidic circuit.
    - **Hardware**:
        - **ADuCM355**: Detailed specifications and usage of the ADuCM355 microcontroller.
        - **STM32WB55RG**: Detailed specifications and usage of the STM32WB55RG microcontroller.
    - **Software**:
        - **Backend**: Information on the backend software development.
        - **Frontend**: Information on the frontend software development.
    - **Code explanation**:
        - **STM32 code**: explanation of the code used in the STM32 microcontroller.
        - **ADuCM355 code**: explanation of the code used in the ADuCM355 microcontroller.
    - **Communication protocol**:
        - **BLE**: Information on the Bluetooth Low Energy communication protocol.  
5. **Contact**: How to get in touch with the team for support or inquiries.

## Getting Started

To get started with this project, follow the steps outlined in the [Project](project.md) section. This will guide you through assembling the hardware components, installing the necessary software, and running your first measurements.

## Further Information

For more detailed guidance on the different aspects of the project, refer to the respective sections listed above. Each section provides in-depth information and instructions to help you understand and replicate the project.

Thank you for visiting the AgroSense project documentation. We hope you find it helpful and informative. If you have any questions or need further assistance, please refer to the [Contact](contact.md) section.

---

*Created with MkDocs.*
